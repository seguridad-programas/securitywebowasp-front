import { ERoutePath } from "routes/routes";
import { IBase } from "./Base.type";

export interface IRoutePath {
  route?: ERoutePath;
  childrens?: IRoutePath[];
  displayName?: string;
  icon?: string;
  root?: boolean;
}

export enum ESchema {
  "VENDOR" = "VENDOR",
  "USER" = "USER",
  "ROLE" = "ROLE",
}

export interface IPermission {
  entity: ESchema;
  manage: boolean;
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
}

export interface IRole extends IBase {
  name: string;
  vendor: string;
  deletable?: boolean;
  editable?: boolean;
  routes: ERoutePath[];
  homeRoute?: ERoutePath;
  permissions: IPermission[];
}
