import Document, { Head, Html, Main, NextScript } from "next/document";
import React from "react";

class ExtendedDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <link
            href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700&display=optional"
            rel="stylesheet"
          />
        </Head>
        <body className="bg-white text-gray-900 dark:bg-black dark:text-white">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default ExtendedDocument;

export async function getServerSideProps(context) {
  const initialProps = await Document.getInitialProps(context);
  return { ...initialProps };
}
