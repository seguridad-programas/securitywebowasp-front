import useTranslation from "next-translate/useTranslation";
import React from "react";
import Lottie from "react-lottie";
import { Button } from "rsuite";
import lottieNotFound from "utils/lottie/not-found.json";

import { ButtonWrapper, NoResultWrapper } from "./NoResult.style";

type UploadFileLottieProps = {
  id?: string;
  onClick?: () => void;
  hideButton?: boolean;
  style?: any;
  size?: number;
  message?: string;
};

const lottieNotFoundOptions = {
  loop: true,
  autoplay: true,
  isClickToPauseDisabled: true,
  animationData: lottieNotFound,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const UploadFileLottie: React.FC<UploadFileLottieProps> = ({
  id,
  onClick,
  hideButton = false,
  style,
  size = 120,
  message = "",
}) => {
  const { t } = useTranslation("common");

  return (
    <NoResultWrapper id={id} style={style}>
      <Lottie options={lottieNotFoundOptions} height={size} width={size} />

      <h6>{message || t("tables.notFoundMessage")}</h6>

      {hideButton ? (
        <ButtonWrapper>
          <div onClick={onClick} aria-hidden="true">
            <Button>{t("tables.tryAgainMessage")}</Button>
          </div>
        </ButtonWrapper>
      ) : null}
    </NoResultWrapper>
  );
};

export default UploadFileLottie;
