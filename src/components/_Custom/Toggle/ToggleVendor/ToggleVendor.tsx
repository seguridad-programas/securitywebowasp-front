import { useMutation } from "@apollo/client";
import { useProfile } from "context/profile/profile.context";
import { gqlUser, gqlVendor } from "gql";
import { IUpdateUserSelectedVendorInput } from "gql/User/mutations";
import { SelectPicker } from "rsuite";
import { VENDOR_ID_PERSIST } from "settings/constants";
import { getQueryOperator } from "utils/helpers";

const ToggleVendor = () => {
  const [updateUserSelectedVendor] = useMutation<{
    updateUserSelectedVendor: IUpdateUserSelectedVendorInput;
  }>(gqlUser.mutations.UPDATE_USER_SELECTED_VENDOR, {
    awaitRefetchQueries: true,
    refetchQueries: [
      getQueryOperator(gqlVendor.queries.GET_VENDOR),
      getQueryOperator(gqlUser.queries.GET_FULL_USER),
    ],
  });

  const { user } = useProfile();

  const handleOnChangeVendor = async (vendor: string) => {
    if (vendor !== localStorage.getItem(VENDOR_ID_PERSIST)) {
      await updateUserSelectedVendor({
        variables: {
          updateUserSelectedVendorInput: { vendor },
        },
      });

      localStorage.setItem(VENDOR_ID_PERSIST, vendor);
      window.location.reload();
    }
  };

  return (
    <>
      {user?.vendorList?.length > 1 && (
        <SelectPicker
          className="mr-2"
          data={user?.vendorList}
          labelKey="name"
          valueKey="id"
          cleanable={false}
          searchable={false}
          onChange={handleOnChangeVendor}
          style={{ width: 150 }}
          defaultValue={
            localStorage.getItem(VENDOR_ID_PERSIST) || user?.selectedVendor.id
          }
          placeholder="Seleccione"
        />
      )}
    </>
  );
};

export default ToggleVendor;
