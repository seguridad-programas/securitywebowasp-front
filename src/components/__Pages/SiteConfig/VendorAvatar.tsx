import Icon from "components/_Custom/Icon/Icon";
import { IVendor } from "types/Vendor.types";

interface IProps {
  logo: string;
  vendor: IVendor;
  size?: number;
}

export const VendorAvatar = ({ vendor, logo, size = 50 }: IProps) => (
    <div
      className="flex items-center relative"
      style={{ width: size, height: size }}
    >
      <div className="absolute inset-0 z-40 flex flex-wrap content-center justify-center opacity-0 hover:opacity-100 hover:bg-purple-300 rounded-full">
        <Icon icon="camera" size="2x" />
      </div>
      {logo || vendor?.logo ? (
        <div
          style={{
            backgroundImage: `url(${logo || vendor?.logo})`,
            width: size,
            height: size,
            borderRadius: "50%",
            backgroundPosition: "center",
            backgroundSize: "cover",
          }}
          className="border-2 border-current-500"
        />
      ) : (
        <div className="text-black bg-white bg-center bg-cover border-2 rounded-full border-current-500 dark:bg-gray-900 dark:text-white w-full h-full">
          <div
            className="absolute inset-0 flex items-center justify-center font-semibold text-md"
            style={{ fontSize: size * 0.32 }}
          >
            <p className="capitalize ml-2">
              {`${vendor?.name?.charAt(0).toUpperCase() || ""}${
                vendor?.name?.split(" ").length > 1
                  ? vendor?.name.split(" ").at(1)?.charAt(0).toUpperCase()
                  : ""
              }`}
            </p>
          </div>
        </div>
      )}
    </div>
  );
