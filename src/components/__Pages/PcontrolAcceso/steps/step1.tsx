
import { Panel } from "rsuite";

interface IPageProps {}

const Step1 = (props: IPageProps) => {
  return (
    <div>


      <Panel className="bg-gray-900">
              <div className="relative isolate overflow-hidden bg-white px-6 py-24 sm:py-32 lg:overflow-visible lg:px-0">

                <div className="mx-auto grid max-w-2xl grid-cols-1 gap-y-16 gap-x-8 lg:mx-0 lg:max-w-none lg:grid-cols-1 lg:items-start lg:gap-y-10">
                  <div className="lg:col-span-2 lg:col-start-1 lg:row-start-1 lg:mx-auto lg:grid lg:w-full lg:max-w-7xl lg:grid-cols-2 lg:gap-x-8 lg:px-8">
                    <div className="lg:pr-4">
                      <div className="lg:max-w-lg">
                        <p className="text-base font-semibold leading-7 text-indigo-600">Owasp Top 10</p>
                        <h1 className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Pérdida de Control de Acceso</h1>
                        <p className="mt-6 text-xl leading-8 text-justify text-gray-700">La pérdida de control de acceso se refiere a una situación en la que un usuario no autorizado 
                        es capaz de obtener acceso a un recurso protegido</p>
                      </div>
                    </div>
                  </div>
                  <div className="-mt-12 -ml-12 p-12 lg:sticky lg:top-4 lg:col-start-2 lg:row-span-2 lg:row-start-1 lg:overflow-hidden">
                    <div className="w-[48rem] max-w-none rounded-xl bg-gray-900 shadow-xl ring-1 ring-gray-400/10 sm:w-[57rem]"></div> 
                    <img src="https://impulso06.com/wp-content/uploads/2023/02/tipos-de-amenazas-ciberseguridad.png" height="400" width="400"/> <br></br>
                    <br></br><br></br><br></br> <br></br><img src="https://owasp.org/Top10/assets/mapping.png" height="500" width="500"/>
                  </div>
                  <div className="lg:col-span-2 lg:col-start-1 lg:row-start-2 lg:mx-auto lg:grid lg:w-full lg:max-w-7xl lg:grid-cols-2 lg:gap-x-8 lg:px-8">
                    <div className="lg:pr-4">
                      <div className="max-w-xl text-base text-justify leading-7 text-gray-700 lg:max-w-lg">
                        <p>Esto podría permitir al atacante obtener acceso a información confidencial, modificar datos, realizar 
                          acciones maliciosas en el sistema o incluso tomar el control total del sistema.</p>
                        <ul role="list" className="mt-8 space-y-8 text-gray-600">
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M5.5 17a4.5 4.5 0 01-1.44-8.765 4.5 4.5 0 018.302-3.046 3.5 3.5 0 014.504 4.272A4 4 0 0115 17H5.5zm3.75-2.75a.75.75 0 001.5 0V9.66l1.95 2.1a.75.75 0 101.1-1.02l-3.25-3.5a.75.75 0 00-1.1 0l-3.25 3.5a.75.75 0 101.1 1.02l1.95-2.1v4.59z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">Configuración incorrecta.</strong> En algunos casos, una pérdida de control de acceso puede ser el resultado de una
                             configuración incorrecta. Por ejemplo, si un administrador de sistemas configura incorrectamente los permisos de un usuario o grupo de usuarios, esto podría permitir
                              que un usuario no autorizado acceda a recursos protegidos.</span>
                          </li>
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">Implementación descuidada de controles de acceso.</strong> Otra razón común de pérdida de control de acceso es una 
                            implementación descuidada de los controles de acceso. Esto podría incluir la falta de autenticación adecuada antes de otorgar acceso a 
                            recursos protegidos o una aplicación que no verifica correctamente los permisos de los usuarios antes de conceder acceso a ciertos recursos.</span>
                          </li>
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path d="M4.632 3.533A2 2 0 016.577 2h6.846a2 2 0 011.945 1.533l1.976 8.234A3.489 3.489 0 0016 11.5H4c-.476 0-.93.095-1.344.267l1.976-8.234z" />
                              <path fill-rule="evenodd" d="M4 13a2 2 0 100 4h12a2 2 0 100-4H4zm11.24 2a.75.75 0 01.75-.75H16a.75.75 0 01.75.75v.01a.75.75 0 01-.75.75h-.01a.75.75 0 01-.75-.75V15zm-2.25-.75a.75.75 0 00-.75.75v.01c0 .414.336.75.75.75H13a.75.75 0 00.75-.75V15a.75.75 0 00-.75-.75h-.01z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">Falta de mantenimiento.</strong> la falta de mantenimiento adecuado también puede ser una razón
                             de la pérdida de control de acceso. Si los controles de acceso no se revisan y actualizan periódicamente,
                              es posible que los permisos otorgados a los usuarios ya no sean apropiados. Por ejemplo, si un empleado cambia de rol dentro de una 
                              organización y se le otorgan nuevos permisos, pero sus permisos anteriores no se eliminan, esto podría permitir que el usuario acceda 
                              a recursos que ya no debería tener acceso.</span>
                          </li>
                        </ul>
                        <p className="mt-8">Además, es importante asegurarse de que los controles de acceso se implementen en todas las partes del sistema, 
                        incluyendo la capa de presentación, la capa de aplicación y la capa de base de datos.</p>
                        <h2 className="mt-16 text-2xl font-bold tracking-tight text-gray-900">No server? No problem.</h2>
                        <p className="mt-6">Id orci tellus laoreet id ac. Dolor, aenean leo, ac etiam consequat in. Convallis arcu ipsum urna nibh. Pharetra, euismod vitae interdum mauris enim, consequat vulputate nibh. Maecenas pellentesque id sed tellus mauris, ultrices mauris. Tincidunt enim cursus ridiculus mi. Pellentesque nam sed nullam sed diam turpis ipsum eu a sed convallis diam.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

      </Panel>
    </div>
  );
};

export default Step1;
