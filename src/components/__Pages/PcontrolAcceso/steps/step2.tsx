import { Panel } from "rsuite";
interface IPageProps {}

const Step2 = (props: IPageProps) => {
  return (
    <div>
      <span className="text-xl">Step 2</span>
      <Panel className="bg-gray-900">
                    <div className="relative isolate overflow-hidden bg-white px-6 py-24 sm:py-32 lg:overflow-visible lg:px-0">
     
                <div className="mx-auto grid max-w-2xl grid-cols-1 text-justify gap-y-16 gap-x-8 lg:mx-0 lg:max-w-none lg:grid-cols-1 lg:items-start lg:gap-y-10">
                  <div className="lg:col-span-2 lg:col-start-1 lg:row-start-1 lg:mx-auto lg:grid lg:w-full lg:max-w-7xl lg:grid-cols-2 lg:gap-x-8 lg:px-8">
                    <div className="lg:pr-4">
                      <div className="lg:max-w-lg">
                        <p className="text-base font-semibold leading-7 text-indigo-600">Owasp Top 10</p>
                        <h1 className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">¿Como evitar la Pérdida de Control de Acceso?</h1>
                        <p className="mt-6 text-xl leading-8 text-gray-700">Para evitar la pérdida de control de acceso, es importante implementar controles de acceso adecuados. 
                        A continuación, te presento algunas medidas que se pueden tomar para evitar esta vulnerabilidad</p>
                      </div>
                    </div>
                  </div>
                  <div className="-mt-12 -ml-12 p-12 lg:sticky lg:top-4 lg:col-start-2 lg:row-span-2 lg:row-start-1 lg:overflow-hidden">
                    <div className="w-[48rem] max-w-none rounded-xl bg-gray-900 shadow-xl ring-1 ring-gray-400/10 sm:w-[57rem]"  ></div>
                    <br></br> <img src="https://storage.googleapis.com/resources232/cacceso.png" height="700" width="700"/> 
                  
                  </div>
                  <div className="lg:col-span-2 lg:col-start-1 lg:row-start-2 lg:mx-auto lg:grid lg:w-full lg:max-w-7xl lg:grid-cols-2 lg:gap-x-8 lg:px-8">
                    <div className="lg:pr-4">
                      <div className="max-w-xl text-base leading-7 text-gray-700 lg:max-w-lg">

                        <ul role="list" className="mt-8 space-y-8 text-gray-600">
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M5.5 17a4.5 4.5 0 01-1.44-8.765 4.5 4.5 0 018.302-3.046 3.5 3.5 0 014.504 4.272A4 4 0 0115 17H5.5zm3.75-2.75a.75.75 0 001.5 0V9.66l1.95 2.1a.75.75 0 101.1-1.02l-3.25-3.5a.75.75 0 00-1.1 0l-3.25 3.5a.75.75 0 101.1 1.02l1.95-2.1v4.59z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">Autenticación y autorización adecuadas</strong> Implementar autenticación adecuada para garantizar que los usuarios solo puedan acceder a los recursos para los que tienen autorización. 
                            Esto podría incluir el uso de contraseñas seguras, autenticación multifactorial o la implementación de sistemas de autenticación como OAuth o OpenID Connect. También se deben implementar mecanismos de autorización 
                            que limiten el acceso de los usuarios solo a los recursos que necesitan para realizar sus tareas.</span>
                          </li>
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">Asignación adecuada de permisos y roles.</strong> Es importante asignar permisos y roles adecuados a los usuarios para garantizar que solo tengan acceso a los recursos que necesitan. 
                            Los permisos y roles se deben otorgar según el principio de menor privilegio, lo que significa que los usuarios solo deben tener los permisos necesarios para realizar sus tareas. También se deben revisar periódicamente los permisos de 
                            los usuarios para garantizar que sigan siendo apropiados.</span>
                          </li>
                        </ul>
                        <p className="mt-8">Existen diversas herramientas que pueden ayudar a prevenir la pérdida de control de acceso.
                        Algunas de ellas son: Sistemas de gestión de identidad y acceso (IAM, por sus siglas en inglés),Sistemas de prevención de intrusiones (IPS, por sus siglas en inglés)
                        Herramientas de análisis de vulnerabilidades y Herramientas de gestión de configuración
                        Es importante destacar que estas herramientas no son una solución completa para prevenir 
                        la pérdida de control de acceso, y que es necesario implementar políticas y prácticas de 
                        seguridad adecuadas en toda la organización para garantizar la protección de los recursos.
                         Sin embargo, estas herramientas pueden ser útiles para complementar las medidas de seguridad
                          existentes y mejorar la protección contra la pérdida de control de acceso</p>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>

      </Panel>
    </div>
  );
};

export default Step2;
