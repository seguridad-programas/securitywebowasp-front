
import * as React from 'react';
import { Checkbox,CheckboxGroup,Button,Modal,Placeholder,Message } from 'rsuite';
interface IPageProps {}

const Step3 = (props: IPageProps) => {
  const [checkedOne, setCheckedOne] = React.useState(false);
  const [checkedTwo, setCheckedTwo] = React.useState(false);
  const [checkedThree, setCheckedThree] = React.useState(false);


  const [open, setOpen] = React.useState(false);
  const [bad, setBad] = React.useState(false);
  const [regular, setRegular] = React.useState(false);

  const handleOpen = () => {
    var nota= 0;
    console.log(checkedOne,checkedTwo,checkedThree)
    if(checkedOne == true && checkedTwo == true && checkedThree == true){
      nota = 3;
    }else if (checkedOne == false && checkedTwo == false && checkedThree == false){
       nota = 0;
    }else{
      nota = 2;
    }
    
    if(nota == 3){
      setOpen(true);
    }else if(nota ==2){
      setRegular(true);
    }else if(nota == 0){
      setBad(true);
    }
    
  
  }
  const handleClose = () => {

    setOpen(false);
  }
  const handleCloseBad = () => setBad(false);
  const handleCloseRegular = () => setRegular(false);

  const handleChangeOne = (e) => {

    console.log("ha seleccionado la respuesta A")
    setCheckedOne(!checkedOne);
   
  };

  const handleChangeTwo = (e) => {

    setCheckedTwo(!checkedTwo);
  };

  const handleChangeThree = (e) => {

    setCheckedThree(!checkedThree);
  };
  return (
    <div>
    <br></br>

    <CheckboxGroup name="checkboxList">
    <p>1- Cual de las siguientes opciones ayuda a  evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="A" onChange={e => handleChangeOne(e)}>Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="B">Instalar antivirus</Checkbox>
    <p>2- Cual de las siguientes opciones ayuda a evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="C" onChange={e => handleChangeTwo(e)}>Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="D" >
    Instalar antivirus
    </Checkbox>
    <p>3- Cual de las siguientes opciones ayuda a evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="E" onChange={e => handleChangeThree(e)}>Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="F" >
    Instalar antivirus
    </Checkbox>
  </CheckboxGroup>
  <br></br>
  <Button color="cyan" appearance="primary" onClick={handleOpen}>
        Enviar
      </Button>
    
    
    
      <Modal open={open} onClose={handleClose}>
        <Modal.Header>
          <Modal.Title>Resultado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
              <Message showIcon type="success" header="Puntuación mas Alta">
                    Felicidades Aprobaste el curso !!
              </Message>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleClose} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleClose} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    
      <Modal open={bad} onClose={handleCloseBad}>
        <Modal.Header>
          <Modal.Title>Resultado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Message showIcon type="error" header="Una vez más">
              Animo !! Puedes intentarlo de nuevo.
            </Message>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseBad} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleCloseBad} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal open={regular} onClose={handleCloseRegular}>
        <Modal.Header>
          <Modal.Title>Resultado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <Message showIcon type="info" header="Aprobaste">
             Felicidades!! Haz aprobado el curso. 
            </Message>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseRegular} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleCloseRegular} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    
    </div>


  
  );
};

export default Step3;
