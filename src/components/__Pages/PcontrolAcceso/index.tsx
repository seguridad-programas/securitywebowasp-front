import Header from "components/_Custom/Header/Header";
import { useState } from "react";
import { Button, Row,Steps } from "rsuite";
import Step1 from "./steps/step1";
import Step2 from "./steps/step2";
import Step3 from "./steps/step3";


export const PcontrolAcceso = () => {

  const [step, setStep] = useState(1);
  return (
    <div className="p-6">
      <Header title="Owasp Page" description="PcontrolAcceso Description" />
      <Steps current={step-1}>
        <Steps.Item title="Introduccion" />
        <Steps.Item title="Contenido" />
        <Steps.Item title="Test" />
      </Steps>
      <Row>
        {step === 1 && <Step1 />}
        {step === 2 && <Step2 />}
        {step === 3 && <Step3 />}
      </Row>

      <div className="flex gap-2 mt-10">
        <Button
          className="bg-current text-white"
          onClick={() => setStep((prev) => prev + 1)}
        >
          Siguiente
        </Button>
        <Button
          className="bg-current text-white"
          onClick={() => setStep((prev) => prev - 1)}
        >
          Atras
        </Button>
      </div>
    </div>
  );
};
