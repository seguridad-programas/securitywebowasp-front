import Header from "components/_Custom/Header/Header";
import { useState } from "react";
import { Button, Row, Steps } from "rsuite";
import Step1 from "./steps/step1";
import Step2 from "./steps/step2";

export const OwaspPage = () => {
  const [step, setStep] = useState(1);
  return (
    <div className="p-6">
      <Header title="Owasp Page" description="Owasp Description" />

      <Row>
        {step === 1 && <Step1 />}

        {step === 2 && <Step2 />}
      </Row>

      <div className="flex gap-2 mt-10">
        <Button
          className="bg-current text-white"
          onClick={() => setStep((prev) => prev + 1)}
        >
          Siguiente
        </Button>
        <Button
          className="bg-current text-white"
          onClick={() => setStep((prev) => prev - 1)}
        >
          Atras
        </Button>
      </div>
    </div>
  );
};
