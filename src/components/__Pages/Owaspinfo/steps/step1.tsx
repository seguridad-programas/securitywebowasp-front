import { Panel } from "rsuite";
import Link from "next/link";
import { Col, Row } from "rsuite";
interface IPageProps {}

const Step1 = (props: IPageProps) => {
  return (
    
    <div>
    <Panel className="bg-gray-900">
      <span className="text-xl">Vulnerabilidades Owasp TOP 10</span>
      <div className="px-4 py-5 sm:px-6">
          <h3 className="text-base font-medium leading-6 text-white-900">Cursos Disponibles</h3>

      </div>
      <Row className="mt-6">
        <Col xs={24} md={16} lg={6} className="mb-12">
          <div className="border-t rounded-lg ">
          <dl>
            <div className="bg-gray-50 border rounded-lg px-4 py-5 sm:grid  sm:gap-2 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Pérdida de Control de Acceso</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">sube de la quinta posición a 
              la categoría con el mayor 
              riesgo en seguridad de aplicaciones web; los datos proporcionados indican que, en promedio, 
              el 3,81% de las aplicaciones probadas tenían una o más Common Weakness Enumerations (CWEs) 
              con más de 318.000 ocurrencias de CWEs en esta categoría de riesgo. </dd>
              <div className="ml-4 flex-shrink-0">
                <Link href="/pcontrolacceso" className="font-medium text-black-100 hover:text-indigo-200">
                    <a>INICIAR CURSO</a>
                </Link>
                
              </div>  
            </div> 
          </dl>
          </div> 
        </Col>  

          <Col xs={24} md={16} lg={6} className="mb-12">
            <div className="border-t rounded-lg">
            <dl>
              <div className="bg-gray-50 border rounded-lg px-4 py-5 sm:grid  sm:gap-2 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">Fallas Criptográficas</dt>
                <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">sube una posición ubicándose en la segunda,
                antes conocida como A3:2017-Exposición de Datos Sensibles, que era más una característica que una causa raíz. 
                El nuevo nombre se centra en las fallas relacionadas con la criptografía, como se ha hecho implícitamente antes. 
                Esta categoría frecuentemente conlleva a la exposición de datos confidenciales</dd>
                <div className="ml-4 flex-shrink-0">
                    <Link href="/fallascriptograficas" className="font-medium text-black-200 hover:text-indigo-200">
                      <a>INICIAR CURSO</a>
                  </Link>
                </div>  
              </div> 
            </dl>
            </div> 
          </Col> 

          <Col xs={24} md={16} lg={6} className="mb-12">
          <div className="border-t rounded-lg">
              <dl>
                <div className="bg-gray-50 rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500"> Inyección</dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">Desciende hasta la tercera posición. 
                  El 94% de las aplicaciones fueron probadas con algún tipo de inyección y estas mostraron una tasa de 
                  incidencia máxima del 19%, promedio de 3.37%, y las 33 CWEs relacionadas con esta categoría tienen la 
                  segunda mayor cantidad de ocurrencias en aplicaciones con 274.000 ocurrencias. 
                  La inyección SQL es la vulnerabilidad mas famosa de esta categoria </dd>
                  <Link href="/inyeccion" className="font-medium text-black-200 hover:text-indigo-200">
                        <a>INICIAR CURSO</a>
                    </Link>
                </div> 
              </dl>
          </div> 
          </Col>

          <Col xs={24} md={16} lg={6} className="mb-12">
          <div className="border-t rounded-lg">
          <dl>
            <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
              <dt className="text-sm font-medium text-gray-500"> Diseño Inseguro </dt>
              <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">nueva categoría para la edición 2021, 
              con un enfoque en los riesgos relacionados con fallas de diseño. Si realmente queremos madurar como industria,
              debemos "mover a la izquierda" del proceso de desarrollo las actividades de seguridad. Necesitamos más modelos
                de amenazas, patrones y principios con diseños seguros y arquitecturas de referencia. Un diseño inseguro no 
                puede ser corregida con</dd>
                <Link href="/disenoinseguro" className="font-medium text-black-200 hover:text-indigo-200">
                    <a>INICIAR CURSO</a>
                </Link>
            </div> 
          </dl>
          </div>
          
          </Col>
        </Row>

        <Row className="mt-6">
          <Col xs={24} md={16} lg={6} className="mb-11">
          <div className="border-t rounded-lg">
              <dl>
                <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">  Configuración de Seguridad Incorrecta </dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">asciende desde la sexta posición en la edición anterior;
                  el 90% de las aplicaciones se probaron para detectar algún tipo de configuración incorrecta, con una tasa de incidencia 
                  promedio del 4,5% y más de 208.000 casos de CWEs relacionadas con esta categoría de riesgo. Con mayor presencia de software 
                  altamente configurable.</dd>
                  <Link href="/cseguridadincorrecto" className="font-medium text-black-200 hover:text-indigo-200">
                        <a>INICIAR CURSO</a>
                    </Link>
                </div> 
              </dl>
              </div> 
          </Col>
          <Col xs={24} md={16} lg={6} className="mb-11">
          <div className="border-t rounded-lg">
            <dl>
              <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">   Componentes Vulnerables y Desactualizados  </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">antes denominado como Uso de Componentes con Vulnerabilidades Conocidas,
                ocupa el segundo lugar en el Top 10 de la encuesta a la comunidad, pero también tuvo datos suficientes para estar en el Top 10 a través 
                del análisis de datos. Esta categoría asciende desde la novena posición en la edición 2017 . </dd>
                  <Link href="/cvulnerablesdesactualizados" className="font-medium text-black-200 hover:text-indigo-200">
                      <a>INICIAR CURSO</a>
                  </Link>
              </div> 
            </dl>
            </div>
          </Col>
          <Col xs={24} md={16} lg={6} className="mb-12">
          <div className="border-t rounded-lg">
              <dl>
                <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">   Fallas de Identificación y Autenticación   </dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">previamente denominada como Pérdida de Autenticación, descendió desde la 
                  segunda posición, y ahora incluye CWEs que están más relacionadas con fallas de identificación. Esta categoría sigue siendo una parte 
                  integral del Top 10, pero el incremento en la disponibilidad de frameworks estandarizados parece estar ayudando.</dd>
                  <Link href="/fidentificacionautenticacion" className="font-medium text-black-200 hover:text-indigo-200">
                        <a>INICIAR CURSO</a>
                    </Link>  
                </div> 
              </dl>
              </div> 
          </Col>
          
          <Col xs={24} md={16} lg={6} className="mb-12">

          <div className="border-t rounded-lg">
            <dl>
              <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">   Fallas en el Software y en la Integridad de los Datos  </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">es una nueva categoría para la edición 2021, que se centra en hacer 
                suposiciones relacionadas con actualizaciones de software, los datos críticos y los pipelines CI/CD sin verificación de integridad.
                Corresponde a uno de los mayores impactos según los sistemas de ponderación de vulnerabilidades 
                (CVE/CVSS).
</dd>
                  <Link href="/fsoftwareintegridaddatos" className="font-medium text-black-200 hover:text-indigo-200">
                      <a>INICIAR CURSO</a>
                  </Link>
              </div> 
            </dl>
            </div> 
          </Col>
        </Row>  
          <Col xs={24} md={16} lg={6} className="mb-12">

                      <div className="border-t rounded-lg">
                  <dl>
                    <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
                      <dt className="text-sm font-medium text-gray-500">  Fallas en el Registro y Monitoreo   </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">previamente denominada como A10:2017-Registro y
                      Monitoreo Insuficientes, es adicionada desde el Top 10 de la encuesta a la comunidad (tercer lugar) y ascendiendo
                        desde la décima posición de la edición anterior. Esta categoría se amplía para incluir más tipos de fallas,
                        es difícil de probar y no está bien representada en los datos de CVE/CVSS. </dd>
                        <Link href="/fregistromonitoreo" className="font-medium text-black-200 hover:text-indigo-200">
                            <a>INICIAR CURSO</a>
                        </Link> 
                    </div> 
                  </dl>
                  </div> 
          </Col>
          <Col xs={24} md={16} lg={6} className="mb-12">
          <div className="border-t rounded-lg">
            <dl>
              <div className="bg-gray-50 border-t rounded-lg px-4 py-5 sm:grid sm:grid-cols-2 sm:gap-2 sm:px-6">
               <dt className="text-sm font-medium text-gray-500">  Falsificación de Solicitudes del Lado del Servidor  </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">es adicionada desde el Top 10 de la encuesta a la comunidad 
                (primer lugar). Los datos muestran una tasa de incidencia relativamente baja con una cobertura de pruebas por encima del promedio, 
                junto con calificaciones por encima del promedio para la capacidad de explotación e impacto.
      
                </dd>
                <Link href="/fsolicitudesservidor" className="font-medium text-black-200 hover:text-indigo-200">
                      <a>INICIAR CURSO</a>
                  </Link>
              </div> 
            </dl>
            </div> 
          </Col>

 

      </Panel>
    </div>
  );
};

export default Step1;
