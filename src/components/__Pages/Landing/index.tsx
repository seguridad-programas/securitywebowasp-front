import ToggleTheme from "components/_Custom/Toggle/ToggleTheme/ToggleTheme";

import VendorBanner from "./Banner/Banner";
import { CallToAction } from "./CallToAction/CallToAction";
import { Testimonials } from "./Testimonials/Testimonials";

export const LandingPage = () => (
  <div>
    <VendorBanner />
    <div className="w-full text-center mt-6">
      <ToggleTheme />
    </div>
    <CallToAction />
    <Testimonials />
  </div>
);
