import { useTheme } from "next-themes";

export const Footer = () => {
  const { theme } = useTheme();

  return (
    <div className="bottom-0 w-full bg-gray-100 dark:bg-gray-900">
      <div className="pt-12 w-full flex flex-col text-center pb-12">
        {/* <div>
          <Image
            src={theme === "dark" ? LOGO_DARK : LOGO_LIGHT}
            width={300}
            height={50}
            alt="logo"
          />
        </div> */}

        {/* <p>web scaning</p>
        <span>Santiago, Chile.</span> */}
      </div>

      <div className=" w-full text-center text-gray-400 p-6 border-t border-gray-300 dark:border-gray-800">
        <span>Copyright @ Web Scanning {new Date().getFullYear()}</span>
      </div>
    </div>
  );
};
