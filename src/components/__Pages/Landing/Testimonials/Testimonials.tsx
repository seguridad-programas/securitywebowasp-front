import React from "react";
import { Col, Row } from "rsuite";

import { TestimonialCard } from "./Cards/TestimonialCard";

export const Testimonials = () => (
  <div className="py-12 container mx-auto px-3">
    <div className="flex flex-col items-center gap-2 bg-white dark:bg-black">
      <h2 className="text-3xl font-bold  text-opacity-75 text-center">
        Aprende sobre vulnerabilidades web
      </h2>
      <p className="text-lg text-center">
      El OWASP Top 10 es una guía completa para ayudar a
       las organizaciones a comprender los riesgos y amenazas asociados con 
       sus API y cómo protegerlas
      </p>
    </div>
    <Row className="mt-6">
      <Col xs={24} md={12} lg={8} className="mb-12">
        <TestimonialCard
          title="Escáner de Vulnerabilidades"
          description="conocé como funciona nuestro escaner de detección SQL inyection"
          image="https://www.muyseguridad.net/wp-content/uploads/2018/12/Ciberseguridad2018_0-1000x600.jpg"
        />
      </Col>
      <Col xs={24} md={12} lg={8} className="mb-12">
        <TestimonialCard
          title="Introducción a la seguridad Web"
          description="Conoce las mejoras practicas de seguridad web"
          image="https://www.cdmon.com/images/easyblog_articles/1805/b2ap3_large_vulnerabilidades-malware.webp"
        />
      </Col>
      <Col xs={24} md={12} lg={8} className="mb-12">
        <TestimonialCard
          title="Owasp TOP 10"
          description="Conocé cada una de las vulnerabilidades y como protegerte de ellas"
          image="https://i.ytimg.com/vi/49Byc4PyY4E/maxresdefault.jpg"
        />
      </Col>
    </Row>
  </div>
);
