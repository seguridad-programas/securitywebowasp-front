import Link from "next/link";

const mouse = (event) => {
  const el = event.target;
  let colorhex = [
    "#7AF377",
    "#3498DB",
    "#F1C530",
    "#F29C29",
    "#8E44AD",
    "#4AA086",
    "#E74C3C",
    "#65CC71",
    "#D3541B",
    "#EB4367",
    "#74F7D9",
    "#DDA8FC",
  ];
  el.style.color = colorhex[Math.floor(Math.random() * 12)];
};

const onMouseOut = (event) => {
  const el = event.target;
  let white = "#FFFFFF";
  el.style.color = white;
};

export const CallToAction = () => (
  <div className="bg-white dark:bg-black py-12 px-3 w-full container mx-auto">
    <div className="flex flex-col items-center gap-2">
      <h2 className="text-3xl font-bold ">
        Vulnerabilidades de Seguridad Owasp TOP 10
      </h2>
      <p className="text-lg ">Tesis de cyberseguridad</p>
    </div>
    <div className="flex mt-10 justify-center flex-col md:flex-row gap-3">
      <div
        onMouseOver={(event) => mouse(event)}
        className="bg-gray-200 dark:bg-gray-700 flex font-bold items-center justify-center text-4xl rounded-2xl w-full md:w-2/5"
        style={{ height: 400 }}
        onMouseOut={(event) => onMouseOut(event)}
      >
        <Link href="/sqltop10">
          <a>Escáner Inyección de SQL</a>
        </Link>
      </div>
      <div
        onMouseOver={(event) => mouse(event)}
        className=" bg-gray-200 dark:bg-gray-700  flex font-bold items-center justify-center text-4xl rounded-2xl w-full md:w-2/5"
        style={{ height: 400 }}
        onMouseOut={(event) => onMouseOut(event)}
      >
        <Link href="/owaspinfo">
          <a>Documentación</a>
        </Link>
      </div>
    </div>
  </div>
);
