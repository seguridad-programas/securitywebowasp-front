
import * as React from 'react';
import { Checkbox,CheckboxGroup,Button,Placeholder, Modal  } from 'rsuite';
interface IPageProps {}

const Step3 = (props: IPageProps) => {
  const [checkedOne, setCheckedOne] = React.useState(false);
  const [checkedTwo, setCheckedTwo] = React.useState(false);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleChangeOne = () => {
    setCheckedOne(!checkedOne);
  };

  const handleChangeTwo = () => {
    setCheckedTwo(!checkedTwo);
  };
  return (
    <div>
    <br></br>

    <CheckboxGroup name="checkboxList">
    <p>1- Cual de las siguientes opciones ayuda a  evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="A">Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="B">Instalar antivirus</Checkbox>
    <p>2- Cual de las siguientes opciones ayuda a evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="C">Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="D" >
    Instalar antivirus
    </Checkbox>
    <p>2- Cual de las siguientes opciones ayuda a evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="E">Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="F" >
    Instalar antivirus
    </Checkbox>
  </CheckboxGroup>
  <br></br>
  <Button color="cyan" appearance="primary" onClick={handleOpen}>
        Enviar
      </Button>
    
    
    
      <Modal open={open} onClose={handleClose}>
        <Modal.Header>
          <Modal.Title>Reusltado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Placeholder.Paragraph />
          <p>Felicidades Aprobaste el curso </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleClose} appearance="primary">
            SI
          </Button>
          <Button onClick={handleClose} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      </div>
    
  );
};

export default Step3;
