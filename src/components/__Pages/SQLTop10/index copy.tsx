import Header from "components/_Custom/Header/Header";
import { Row } from "rsuite";
import React, { useState, useEffect } from 'react';

interface IPageProps {}




const SQLTop10 = (props: IPageProps) => {
    
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [posts, setPosts] = useState([]);
    const addPost = async (body) => {
               await fetch('http://localhost:3001/sqlapi/vulnerability', {
                 method: 'POST',
                 body: JSON.stringify({
                  
                      "url":"http://testphp.vulnweb.com/artists.php?artist=1"
                  
                 }),
                 headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                 },

               })
               .then((response) => response.json())
                    .then((data) => {
                      setPosts((posts) => [data, ...posts]);
                      setBody('');
                      console.log(data);
                    })
                    .catch((err) => {
                      console.log(err.message);
                    });
    };
    const handleSubmit = (e) => {
      e.preventDefault();
      addPost(body);
   }; 

  return (
    <div className="p-6">
      <Header title="SQL MAP UI" description="SQL Top 10" />
      <div className="mt-5 md:col-span-2 md:mt-0">
            <form onSubmit={handleSubmit}>
              <div className="shadow sm:overflow-hidden sm:rounded-md">
                <div className="space-y-6 bg-white px-4 py-5 sm:p-6">
                  <div className="grid grid-cols-3 gap-6">
                    <div className="col-span-3 sm:col-span-2">
                      <label htmlFor="company-website" className="block text-sm font-medium leading-6 text-gray-900">Website</label>
                      <div className="mt-2 flex rounded-md shadow-sm">
                        <span className="inline-flex items-center rounded-l-md border border-r-0 border-gray-300 px-3 text-gray-500 sm:text-sm">http://</span>
                        <input type="text" value={body} name="company-website" id="company-website" className="block w-full flex-1 rounded-none rounded-r-md border-0 py-1.5 
                        text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                         placeholder="www.example.com" />
                      </div>
                    </div>
                  </div>

        
                  <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
                    <button type="submit" className="inline-flex justify-center rounded-md bg-indigo-600 py-2 px-3 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500
                     focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 
                     focus-visible:outline-indigo-500">Analizar</button>
                  </div>
                </div>  
              </div>
            </form>
      </div>

      <div className="App">
      <tbody>
        <tr>
          <th>Name</th>
          <th>Brand</th>
  
        </tr>
        
        {posts.map((data, value) => (
          <tr key={data}>
            {console.log("data:",data)}
            {data.data.forEach(element => {
             
                {console.log("aqui",JSON.stringify(element.value[0]))}

                <td>{JSON.stringify(element.value[0])}</td>
              
             
            })}
           
     
          </tr>
        ))}
      </tbody>
    </div>
    </div>


         
     
  );
};

export default SQLTop10;
