import NoResult from "components/NoResult/NoResult";
import Header from "components/_Custom/Header/Header";
import Icon from "components/_Custom/Icon/Icon";
import { useState } from "react";
import React from 'react';


import {
  Table,
  Button,
  Col,
  Notification,
  Checkbox, 
  CheckboxGroup,
  Input,
  Radio,
  Animation,
  Panel,
  Drawer,
  List,
  InputGroup,
  Loader,
  Placeholder,
  Slider,
  Modal,
  SelectPicker,
  TransitionProps,
  Row,
} from "rsuite";

interface FadeProps extends TransitionProps {
  innerRef?: React.RefObject<HTMLDivElement>;
}

const { Column, HeaderCell, Cell } = Table;
const labels = ['A', 'B', 'C', 'D'];
const SQLTop10 = () => {
  const [informationSchema, setIinformationschema] = useState([]);
  const [loading, setLoading] = useState(false);
  const [url, setUrl] = useState("");
  const [ip, setIP] = useState("");
  const [email, setEmail] = useState("");
  
  const [threads, setThreads] = useState("");
  
  const [datap, setDatap] = useState([]);
  const [acuart, setAcuart] = useState([]);
  const [log, setLog] = useState([]);
  const [getTables, setGettables] = useState(false);
  const [getAll, setGetall] = useState(false);
  const [getUser, setGetUser] = useState(false);
  const [getPrivileges, setGetPrivilegios] = useState(false);
  const [getHostname, setGetHost] = useState(false);
  const [getPasswordHashes, setGetPasswordHashes] = useState(false);

  const [data, setData] = useState("");
  const [tamper, setTamper] = useState("");
  const [dbms, setDBMS] = useState("");
  const [dbmsversion, setDBMV] = useState("");
  const [os, setOS] = useState("");
  const [taskid, setTaskid] = useState("");
  
  const [slider, setSlider] = useState(0);
  const pref = React.createRef();
  const [show, setShow] = useState(false);
  const onChange = () => setShow(!show);
  const [openWithHeader, setOpenWithHeader] = React.useState(false);
  
  const threadv = ['1','2','3','4','5','6','7','8','9','10'].map(
    item => ({ label: item, value: item })
  );
  const tamperv = ['apostrophemask.py', 'apostrophenullencode.py',
   'appendnullbyte.py', 'base64encode.py', 'between.py', 'bluecoat.py', 'chardoubleencode.py', 'commalesslimit.py',
   'concat2concatws.py',
   'charencode.py',
   'charunicodeencode.py',
   'equaltolike.py',
   'escapequotes.py',
   'greatest.py',
   'halfversionedmorekeywords.py',
   'ifnull2ifisnull.py',
   'modsecurityversioned.py',
   'modsecurityzeroversioned.py',
   'multiplespaces.py',
   'nonrecursivereplacement.py',
   'percentage.py',
   'overlongutf8.py',
   'randomcase.py',
   'randomcomments.py',
   'securesphere.py',
   'sp_password.py',
   'space2comment.py',
   'space2dash.py',
   'space2hash.py',
   'space2morehash.py',
   'space2mssqlblank.py',
   'space2mssqlhash.py',
   'space2mysqlblank.py',
   'space2mysqldash.py',
   'space2plus.py',
   'space2randomblank.py',
   'symboliclogical.py',
   'unionalltounion.py',
   'unmagicquotes.py',
   'uppercase.py',
   'varnish.py',
   'versionedkeywords.py',
   'versionedmorekeywords.py',
   'xforwardedfor.py'
  ].map(
    item => ({ label: item, value: item })
  );

  const [map, setMap] = useState(new Map());
  /**Modal */
  const [open, setOpen] = useState(false);
  const [openData, setOpenData] = useState(false);
  const [openTamper, setOpenTamper] = useState(false);
  const [openThread, setOpenThread] = useState(false);
  const [openDB, setOpenDB] = useState(false);
  
  const handleOpenThreads= () => {
    setOpenThread(true);
  }

  const handleClose = () => {

    setOpen(false);
  }

  const handleOpenDB = () => {
    setOpenDB(true);
  }

  const handleCloseDB = () => {

    setOpenDB(false);
  }

  const handleOpen = () => {
      setOpen(true);
  }

  const handleCloseData = () => {

    setOpenData(false);
  }

  const handleOpenData = () => {
    setOpenData(true);
  }

  const handleCloseTamper = () => {

    setOpenTamper(false);
  }

  const handleOpenTamper = () => {
    setOpenTamper(true);
  }

  const [inicio, setInicio] = useState(true);
  const [segundo, setSegundo] = useState(false);
  const [tercero, setTercero] = useState(false);
  const [cuarto, setCuarto] = useState(false);
  const [quinto, setQuinto] = useState(false);
  const [sexto, setSexto] = useState(false);
  const handleCloseIncio = () => {

    setInicio(false);
    setSegundo(true);
  }

  const handleOpenInicio = () => {
      setInicio(true);
  }

  const handleCloseSegundo = () => {

    setSegundo(false);
    setTercero(true);
  }

  const handleOpenSegundo = () => {
    setSegundo(true);
  }

  const handleCloseTercero = () => {

    setTercero(false);
    setCuarto(true);
  }

  const handleOpenTercero = () => {
    setSegundo(true);
  }
  const handleCloseCuarto = () => {

    setCuarto(false);
    setQuinto(true);
  }

  const handleOpenCuarto = () => {
    setSegundo(true);
  }
  const handleCloseQuinto = () => {

    setQuinto(false);
    setSexto(true);
  }

  const handleOpenQuinto= () => {
    setQuinto(true);
  }
  const handleCloseSexto = () => {

    setSexto(false);
  }

  const handleOpenSexto = () => {
    setSexto(true);
  }

  const handleGetTable = (e) => {

   if(getTables ==false){
   
    setGettables(true);

   }else{
   
    setGettables(false);
   }

  };

  const handleGetAll = (e) => {

   if(getAll==false){
    setGetall(true);
   }else{

    setGetall(false);
   }

  };

  const handleGetPasswordHashes = (e) => {

    if(getPasswordHashes==false){
     setGetPasswordHashes(true);
    }else{
 
     setGetPasswordHashes(false);
    }
 
   };
  const handleGethost = (e) => {

    if(getHostname==false){
      setGetHost(true);
    }else{
 
      setGetHost(false);
    }
 
   };


  const handleGetUser = (e) => {

    if(getUser==false){
     setGetUser(true);
    }else{
 
     setGetUser(false);
    }
 
   };

   const handleGetPrivilegios = (e) => {

    if(getPrivileges==false){
     setGetPrivilegios(true);
    }else{
 
     setGetPrivilegios(false);
    }
 
   };


  /*** */
   const getLogs = (taskid: string) => {
    const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
  
    setLog([]);
     delay(3000);
   
    const urlLogs =  "http://localhost:3001/sqlapi/logs/" + taskid;
    fetch(urlLogs)
    .then((response) => {response.json().then(({log}) =>{

      setLog(log);
     });
     })
    .catch(error => console.error(error));
    setTaskid("")
  }
  
  const Panel = React.forwardRef((props, ref) => (
    <div
      {...props}
      style={{ background: '#000', width: 900, height: 300, overflow: 'hidden' }}
    >
      <Table
                height={400}
                data={log}
                onRowClick={rowData => {
                  console.log(rowData);
                }}
              >
                <Column width={100} align="center" fixed>
                  <HeaderCell>Level</HeaderCell>
                  <Cell dataKey="level" />
                </Column>

                <Column width={650}>
                  <HeaderCell>Mensaje</HeaderCell>
                  <Cell dataKey="message" />
                </Column>

                <Column width={150}>
                  <HeaderCell>Hora</HeaderCell>
                  <Cell dataKey="time" />
                </Column>

              </Table>
    </div>
  ));
/**/

  const backendAPIURL = "http://localhost:3001/sqlapi/vulnerabilitys";

  const handleStartQLI = () => {

   
    var bodyp : string;
    setLog([]);
    setLoading(true);

    if(data != "" && slider == 0 ){
       bodyp = JSON.stringify({
        url,data,tamper,getTables,getAll,threads
      })
    }else if(slider > 0){
      let risk = ""
      let level = ""
       switch(slider){
        case 1:
          risk = "1";
          level="2";
          bodyp = JSON.stringify({
            url,data,risk,level,tamper,getTables,getAll,threads
          })
          break;
        case 2:
          risk = "2";
          level="3";
          bodyp = JSON.stringify({
            url,data,risk,level,tamper,getTables,getAll,threads
          })  
          break;
        case 3:
          risk = "3";
          level="4";
          bodyp = JSON.stringify({
            url,data,risk,level,tamper,getTables,getAll,threads
          })  
          break;
        default:
        
       }
      
    
    }else{
      bodyp = JSON.stringify({
        url,tamper,getTables,getAll,threads
      })
    }
    

   
    // * make a post request to the backend API
    fetch(backendAPIURL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: bodyp,
    })
      .then((res) => {
        if (res.status === 201) {
      
          res.json().then(({ datap,dbms,dbmsversion,os,taskid,informationSchema,acuart,ip,email }) => {
   
           
           
            if (datap === undefined || datap.length == 0 ) {
              datap = [{title:"No se encontraron resultados - Revisa los logs para mas detalle", payload:"No se encontraron resultado", vector:""}]
            }
       
         
            setEmail(email);
            setIP(ip);
            setIinformationschema(informationSchema);
            setAcuart(acuart);
            setDatap(datap);
            setLoading(false);
            setDBMS(dbms);
            setOS(os);
            setDBMV(dbmsversion);
            setTaskid(taskid);
            getLogs(taskid);
          });
          
        }
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });


  };

  return (
    <div className="p-6">
      <Header title="SQL MAP UI" description="SQL Top 10" />
      <Row>
      
        <Col xs={24} className="pb-2">
          <span className="text-xl font-bold text-center ">
            Ingrese su URL para analizar
          </span>
        </Col>
        <Col xs={24} className="flex gap-2 pb-2">
          <div className="flex flex-col" >
            <InputGroup>
              <InputGroup.Addon>
                <Icon icon={"globe"} />{" "}
              </InputGroup.Addon>
              <Input
                value={url}
                onChange={setUrl}
                placeholder="Ingresa una URL"
                style={{ width: "500px" }}
              />
            </InputGroup>
          </div>
          <Button
            type="button"
            onClick={handleStartQLI}
            disabled={url === "" || loading}
            appearance="ghost"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Analizar
          </Button>
          
          Nivel de Agresividad:
          <div style={{ width: 200, marginLeft: 20, marginTop: 10 }}>
                <Slider
                  min={0}
                  max={labels.length - 1}
                  value={slider}
                  className="custom-slider"
                  handleStyle={{
                    borderRadius: 10,
                    color: '#fff',
                    fontSize: 12,
                    width: 42,
                    height: 22,
                  
                  }}
                  graduated
                  tooltip={false}
                  handleTitle={labels[slider]}
                  onChange={setSlider}
                />
               
              </div>
              <div style={{marginLeft: 25, marginTop: 0 }}>
              <Icon onClick={handleOpen} icon={"ghost"} />{" "}
              </div>
           
              <div style={{marginLeft: 35, marginTop: 0 }}>
              Tamper:&nbsp;&nbsp;&nbsp;&nbsp;   
                  <SelectPicker value={tamper} onChange={setTamper} data={tamperv} style={{ width: 224 }} />
                  </div>
                  <div style={{marginLeft: 28, marginTop: 0 }}>
                     <Icon onClick={handleOpenTamper} icon={"ghost"} />{" "}
                  </div>

        </Col>
        <Col>
        <CheckboxGroup name="checkboxList">

              <Checkbox value="A">Data 
              <div style={{marginLeft:45, marginTop: -15 }}>
                     <Icon onClick={handleOpenData} icon={"ghost"} />{" "}
                  </div>
              </Checkbox>
              
              </CheckboxGroup>
              <div style={{marginLeft: 10, marginTop: 0, marginBottom:30, width:250 }}>
              <InputGroup>
                <InputGroup.Addon>
                  <Icon icon={"code"} />{" "}
                </InputGroup.Addon>
                <Input
                  value={data}
                  onChange={setData}
                  placeholder="data"
                
                />
                
              </InputGroup>
              </div>
              <div style={{marginLeft: 280, marginTop: -67, marginBottom:40 }}>
              <Button onClick={() => setOpenWithHeader(true)}>DB info</Button> <Icon onClick={handleOpenDB} icon={"ghost"} />{" "}
              
              
              <div style={{marginLeft:120, marginTop: -35 }}>
              Hilos:&nbsp;&nbsp;&nbsp;   
                  <SelectPicker value={threads} onChange={setThreads} data={threadv} style={{ width: 100 }} />
                  </div>
                  <div style={{marginLeft: 290, marginTop: -25 }}>
                     <Icon onClick={handleOpenThreads} icon={"ghost"} />{" "}
                  </div>

              </div>

            
           
        </Col>

        {loading && (
          <Col xs={24} className="bg-gray-800 rounded-lg p-6">
            <Placeholder.Paragraph rows={8} />
            <Loader center content="loading" />
          </Col>
        )}

        {datap.length === 0 && !loading && (
          <Col xs={24} className="bg-gray-800 rounded-lg p-20">
            <NoResult message="No hay resultados aún" />
          </Col>
        )}

        {!loading && datap.length > 0 && (
          <Col xs={24} className="bg-gray-800 rounded-lg p-8">
               <table>
                 <thead>
                   
            {console.log("informationSchema.length",informationSchema.length)}
                  <div>
                  <Notification style={{ textDecorationColor:'#e6eded', background: '#161d24', borderRadius: 0 }} type="info" header="Base de Base de Datos">
                    {dbms} 
                  </Notification>
                  <Notification style={{ textDecorationColor:'#e6eded', background: '#161d24', borderRadius: 0 }}  header="Version de BD">
                    {dbmsversion}   
                  </Notification>
                  <Notification style={{ textDecorationColor:'#e6eded', background: '#161d24', borderRadius: 0 }}  header="Sistema Operativo">
                   {os}
                  </Notification>
                  <Notification  style={{ textDecorationColor:'#e6eded', background: '#161d24', borderRadius: 0 }} header="IP">
                   {ip}
                  </Notification>
                  <Notification  style={{ textDecorationColor:'#e6eded', background: '#161d24', borderRadius: 0 }} header="Hostname">
                   {email}
                  </Notification>
                  </div>
            
                
                  <br></br>
                 </thead>
                 <tbody>  
            {datap.map((item) => {
          
              
              return (
       
                <>
                <List>
                  <List.Item>TITULO: {item.title}</List.Item>
                  <List.Item>PAYLOAD:{item.payload} </List.Item>
                  <List.Item>VECTOR:{item.vector}</List.Item>

                </List>
                <br></br>
                <br></br>
           
                </>
               
              
              );
           
            })}

          </tbody>

         </table>
         {!loading && informationSchema.length > 0 && ( 
        
        <Table
        height={300}
        data={informationSchema.map(item => ({ schema: item }))}
        onRowClick={rowData => {
          console.log(rowData);
              }}
            >
              <Column width={300} align="center" fixed>
                <HeaderCell>Information Schema</HeaderCell>
                <Cell  dataKey="schema" style={{ textDecorationColor:'#e6eded', background: '#2cb3bf'}}/>

              </Column>

            </Table>
            
       )}
        {!loading && acuart.length > 0 && ( 
        
        <Table
        height={300}
        data={acuart.map(item => ({ schema: item }))}
        onRowClick={rowData => {
          console.log(rowData);
              }}
            >
              <Column width={300} align="center" fixed>
                <HeaderCell>Tablas</HeaderCell>
                <Cell  dataKey="schema" style={{ textDecorationColor:'#e6eded', background: '#2cb3bf'}}/>

              </Column>

            </Table>
            
       )}
          </Col>

        )}


      </Row>
                <div  style={{marginTop: 15, marginBottom:40 }} className="row">
                <Button onClick={onChange}>Logs</Button>
                <hr />
                <Animation.Fade in={show}>{(props) => <Panel {...props} ref={pref} />}</Animation.Fade>
              </div>


              <Modal open={openDB} onClose={handleCloseDB}>
        <Modal.Header>
          <Modal.Title>Info DB </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             El conjunto de parámetros de "DB data" en sqlmap se utiliza<br></br>
              para especificar la información necesaria para conectarse a una base de datos.  <br></br><br></br>
             Sintaxis Sql Map: <br></br>
                --dbms <br></br>
                --hostname <br></br>
                --port <br></br>
                --password <br></br>
                --db <br></br>
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseDB} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleCloseDB} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>



        <Modal open={openData} onClose={handleCloseData}>
        <Modal.Header>
          <Modal.Title>Data </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             Especifica los datos de una solicitud HTTP POST en formato URL codificado <br></br>
             que se enviará al objetivo durante las pruebas de inyección de SQL <br></br><br></br>
             Sintaxis Sql Map: <br></br>
                --data= params<br></br>
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseData} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleCloseData} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

       

      <Modal open={open} onClose={handleClose}>
        <Modal.Header>
          <Modal.Title>Nivel de Agresividad</Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             Ajusta el grado de agresividad de las pruebas de inyección de SQL<br></br> que sqlmap realiza contra un objetivo<br></br><br></br>
             1 - Bit Harder (100-200 solicitudes) <br></br>
             2 - Good Request (200-500 solicitudes) <br></br>
             3 - Extensive test ( 500 -1000 solicitudes) <br></br><br></br>
             Sintaxis Sql Map: <br></br>
                --risk=3 #MAX<br></br>
                --level=5 #MAX<br></br>
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleClose} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleClose} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal open={openTamper} onClose={handleCloseTamper}>
        <Modal.Header>
          <Modal.Title>Tamper</Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             Los scripts de manipulación personalizados son útiles cuando el 
             objetivo tiene<br></br>
              medidas de seguridad adicionales que evitan que sqlmap funcione correctamente. <br></br>
              Sintaxis SqlMap: <br></br>
              --tamper=name_of_the_tamper <br></br>
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseTamper} appearance="primary">
            Ok
          </Button>
          <Button onClick={handleCloseTamper} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>


      <Modal overflow={true} open={inicio} onClose={handleCloseIncio} style={{ position: 'absolute', top: '50px', left: '250px' }} backdrop={true}>
        <Modal.Header>
          <Modal.Title>TUTORIAL RÁPIDO </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
            SQLMAP UI - Un escaneo practico al alcance de un Click <br></br> 
            
            <b>Guia de Uso:</b> <br></br><br></br>
            
            * &nbsp; <Icon onClick={handleOpen} icon={"ghost"} />{" "} &nbsp; &nbsp; 
            Este icono abre la ventana de ayuda. <br></br> La ventana de ayuda, contiene mas información sobre la funcionalidad 

            <br></br><br></br>
            * &nbsp; Recuerda consultar los logs si quieres conocer mas detalles

            <br></br><br></br>
            * &nbsp; Si quieres profundizar en la herramientas puedes consultar https://sqlmap.org/


              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseIncio} appearance="primary">
            Empezar
          </Button>
        </Modal.Footer>
      </Modal>
      
      <Modal overflow={false} open={segundo} onClose={handleCloseSegundo} style={{ position: 'absolute', top: '100px', left: '60px' }} backdrop={false}>
        <Modal.Header>
          <Modal.Title>INFORMACIÓN </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             &nbsp;Aqui podras ingresar la URL de la web que quieres analizar<br></br><br></br>

            &nbsp; &nbsp;Sintaxis SQLMap: <br></br>
             
            &nbsp; &nbsp;sqlmap -u 'Dirección URL'  <br></br>
            
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseSegundo} appearance="primary">
            Siguiente
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal overflow={false} open={tercero} onClose={handleCloseTercero} style={{ position: 'absolute', top: '140px', left: '60px' }} backdrop={false}>
        <Modal.Header>
          <Modal.Title>&nbsp;INFORMACIÓN </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             &nbsp;<b>DATA:</b> <br></br> &nbsp;En sqlmap se utiliza para especificar <br></br>
             &nbsp;los datos que se enviarán en una solicitud HTTP POST.  <br></br><br></br>
            
            &nbsp;<b>DB Info:</b> <br></br> &nbsp;Abre el panel de opciones para escaneo de Base de Datos <br></br>
            &nbsp;La herramienta puede ser configurada para realizar pruebas específicas<br></br> 
            &nbsp;en la base de datos, como la enumeración de bases de datos, tablas y columnas. <br></br><br></br>
         

           &nbsp;<b>HILOS:</b><br></br> &nbsp;Existen analisis que requieren muchas pruebas para acelerar la velocidad <br></br>
           &nbsp;Aumenta el numero de hilos (Peticiones concurrentes) <br></br><br></br><br></br>

              </p>

       
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseTercero} appearance="primary">
            Siguiente
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal overflow={false} open={cuarto} onClose={handleCloseCuarto} style={{ position: 'absolute', top: '100px', left: '310px',  }} backdrop={false}>
        <Modal.Header>
          <Modal.Title>INFORMACIÓN </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             &nbsp;El nivel de agresividad no ayuda a configurar la cantidad y rigurosidad <br></br>
             &nbsp;de las pruebas a realizar esta función incluye los parámetros:<br></br><br></br>
             &nbsp;--risk , --level de SQLMAP  <br></br>
            
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseCuarto} appearance="primary">
            Siguiente
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal overflow={false} open={quinto} onClose={handleCloseQuinto} style={{ position: 'absolute', top: '100px', left: '450px' }} backdrop={false}>
        <Modal.Header>
          <Modal.Title>INFORMACIÓN </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             &nbsp;Tamper o sabotaje, es una función que permite personalizar las técnicas <br></br> 
             &nbsp;de inyección utilizadas para explotar vulnerabilidades de inyección de SQL<br></br> 
            
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseQuinto} appearance="primary">
            Siguiente
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal overflow={false} open={sexto} onClose={handleCloseSexto} style={{ position: 'absolute', top: '310px', left: '60px' }} backdrop={false}>
        <Modal.Header>
          <Modal.Title>INFORMACIÓN </Modal.Title>
        </Modal.Header>
        <Modal.Body>
             <p>
             &nbsp;Siempre podras consultar los logs y ver más detalles sobre lo que está haciendo<br></br> 
             &nbsp;la aplicación<br></br> <br></br> 
             &nbsp;La ventana de resultados mostrará toda la información encontrada<br></br> 
              </p>
                    
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleCloseSexto} appearance="primary">
            Siguiente
          </Button>
        </Modal.Footer>
      </Modal>


      <Drawer open={openWithHeader} onClose={() => setOpenWithHeader(false)}>
        <Drawer.Header>
          <Drawer.Title>Información de Base de Datos</Drawer.Title>
          <Drawer.Actions>
            <Button onClick={() => setOpenWithHeader(false)}>Cancel</Button>
            <Button onClick={() => setOpenWithHeader(false)} appearance="primary">
              Confirm
            </Button>
          </Drawer.Actions>
        </Drawer.Header>
        <Drawer.Body>
              <>
              <Radio checked={getTables} onClick={handleGetTable}> Tablas</Radio>
              <Radio checked={getAll} onClick={handleGetAll}> Toda la Información</Radio>
              <Radio checked={getUser} onClick={handleGetUser}> Usuarios</Radio><br></br>
              <Radio checked={getPasswordHashes} onClick={handleGetPasswordHashes}> Contraseñas</Radio> 
              <Radio checked={getPrivileges} onClick={handleGetPrivilegios}> Privilegios</Radio>
              <Radio checked={getHostname} onClick={handleGethost}> Hostname</Radio>
            </>
            <div style={{marginLeft: -18, marginTop: -45 }}>
                     <Icon onClick={handleOpen} icon={"ghost"} />{" "}
                  </div>
        </Drawer.Body>
      </Drawer>

    </div>



  );
};

export default SQLTop10;
