import { useMutation } from "@apollo/client";
import Header from "components/_Custom/Header/Header";
import { useDrawer } from "context/drawer/drawer.provider";
import { useProfile } from "context/profile/profile.context";
import { gqlRole } from "gql";
import {
  ICreateRoleInput,
  ICreateRoleResponse,
  IUpdateRoleInput,
  IUpdateRoleResponse,
} from "gql/Role/mutations";
import { IGetRolesResponse } from "gql/Role/queries";
import useTranslation from "next-translate/useTranslation";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import {
  Button,
  Col,
  Input,
  Row,
  SelectPicker,
  TagPicker,
  Toggle,
} from "rsuite";
import { NAME_VALIDATION_REGEX } from "settings/constants";
import { ESchema, IPermission, IRole } from "types/Role.type";
import { v4 as uuid } from "uuid";
import { ERoutePath, PRIVATE_ROUTES } from "../../../../routes/routes";

interface IRoleForm {
  role?: IRole;
}

const CreateOrUpdateRole = ({ role }: IRoleForm) => {
  const { t } = useTranslation("common");
  const { t: tmenu } = useTranslation("menu");

  const { user } = useProfile();
  const { closeDrawer } = useDrawer();
  const tempId: string = uuid();

  const genPermissions = () => {
    const permissions: IPermission[] = [];

    Object.keys(ESchema).forEach((schema: ESchema) => {
      const roleSchemaPermissions = role?.permissions?.find(
        (permission) => permission.entity === schema,
      );

      permissions.push({
        entity: schema,
        manage: roleSchemaPermissions?.manage || false,
        create: roleSchemaPermissions?.create || false,
        read: roleSchemaPermissions?.read || false,
        update: roleSchemaPermissions?.update || false,
        delete: roleSchemaPermissions?.delete || false,
      });
    });
    return permissions;
  };

  const {
    handleSubmit: handleCreateRole,
    control: roleControl,
    formState: { errors },
    setValue,
    watch,
  } = useForm<ICreateRoleInput>({
    defaultValues: {
      name: role?.name || "",
      routes: role?.routes || [],
      homeRoute: role?.homeRoute || ERoutePath.PROFILE,
      permissions: genPermissions(),
    },
  });

  const { fields } = useFieldArray({
    control: roleControl,
    name: "permissions",
  });

  const [createRole, { loading: createRoleLoading }] = useMutation<
    ICreateRoleResponse,
    { createRoleInput: ICreateRoleInput }
  >(gqlRole.mutations.CREATE_ROLE, {
    update: (cache, { data }) => {
      if (data) {
        const { createRole } = data;

        if (createRole) {
          const { getAllRoles } = cache.readQuery<IGetRolesResponse>({
            query: gqlRole.queries.GET_ALL_ROLES,
          });

          if (getAllRoles) {
            cache.writeQuery({
              query: gqlRole.queries.GET_ALL_ROLES,
              data: {
                getAllRoles: [...getAllRoles, createRole],
              },
            });
          }
        }
      }
    },
    onError(e) {
      toast.error(e.message);
    },
  });

  const [updateRole, { loading: updateRoleLoading }] = useMutation<
    IUpdateRoleResponse,
    { updateRoleInput: IUpdateRoleInput }
  >(gqlRole.mutations.UPDATE_ROLE, {
    update: (cache, { data }) => {
      if (data) {
        const { updateRole } = data;

        if (updateRole) {
          const { getAllRoles } = cache.readQuery<IGetRolesResponse>({
            query: gqlRole.queries.GET_ALL_ROLES,
          });

          if (getAllRoles) {
            const patchRoles = getAllRoles.map((r: IRole) =>
              r.id === updateRole.id ? updateRole : r,
            );
            cache.writeQuery({
              query: gqlRole.queries.GET_ALL_ROLES,
              data: {
                getAllRoles: patchRoles,
              },
            });
          }
        }
      }
    },
    onError(e) {
      toast.error(e.message);
    },
  });

  const handleCreateOrUpdate = async (data: ICreateRoleInput) => {
    try {
      if (role)
        await updateRole({
          variables: { updateRoleInput: { id: role?.id, ...data } },
          optimisticResponse: {
            updateRole: {
              id: role?.id,
              name: data.name,
              routes: data.routes,
              vendor: user.selectedVendor.id,
              createdAt: new Date(),
              deletable: true,
              editable: true,
              homeRoute: data.homeRoute,
              permissions: data.permissions,
            },
          },
        });
      else
        await createRole({
          variables: { createRoleInput: { id: tempId, ...data } },
          optimisticResponse: {
            createRole: {
              id: tempId,
              name: role?.name || data.name,
              routes: role?.routes || data.routes,
              vendor: user.selectedVendor.id,
              createdAt: new Date(),
              deletable: true,
              editable: true,
              homeRoute: data.homeRoute,
              permissions: data.permissions,
            },
          },
        });
    } catch (error) {
      console.error("👀 ~ error", error);
    } finally {
      if (role) toast.success(t("roles.forms.updateSuccess"));
      else toast.success(t("roles.forms.creationSuccess"));

      closeDrawer();
    }
  };

  const routePickerData = [];

  Object.keys(PRIVATE_ROUTES).forEach((route) => {
    routePickerData.push({
      label: tmenu(route),
      value: route,
    });
  });

  return (
    <div className="w-full p-6">
      <Header
        {...{
          title: role
            ? t("roles.forms.updateRoleTitle")
            : t("roles.forms.createRoleTitle"),
          description: role
            ? t("roles.forms.updateRoleDescription")
            : t("roles.forms.createRoleDescription"),
        }}
      />
      <form onSubmit={handleCreateRole(handleCreateOrUpdate)}>
        <Row>
          <Col xs={24}>
            <label>{t("roles.forms.name")}</label>
            <Controller
              name="name"
              defaultValue={role?.name || ""}
              rules={{
                required: true,
                pattern: {
                  value: NAME_VALIDATION_REGEX,
                  message: t("regex.nameValidation"),
                },
              }}
              control={roleControl}
              render={({ field }) => (
                <Input
                  {...field}
                  size="lg"
                  className="w-full mt-2"
                  placeholder={t("roles.forms.namePlaceholder")}
                />
              )}
            />
            {errors && errors.name && errors.name.type !== "pattern" && (
              <small className="w-full text-red-500 mb-2">
                {t("roles.forms.roleRequired")}
              </small>
            )}
            {errors && errors.name && errors.name.type === "pattern" && (
              <small className="w-full text-red-500 mb-2">
                {errors.name.message}
              </small>
            )}
          </Col>
          <Col xs={24} className="mt-2">
            <label>{t("roles.forms.routes")}</label>
            <Controller
              name="routes"
              control={roleControl}
              rules={{ required: true }}
              render={({ field }) => (
                <TagPicker
                  {...field}
                  size="lg"
                  className="w-full"
                  data={routePickerData}
                  labelKey="label"
                  valueKey="value"
                  placeholder={t("roles.forms.routesPlaceholder")}
                />
              )}
            />
            {errors && errors.routes && (
              <small className="w-full text-red-500 mb-2">
                {t("roles.forms.routesRequired")}
              </small>
            )}
          </Col>
          <Col xs={24} className="mt-2">
            <label>{t("roles.forms.homeRoute")}</label>
            <Controller
              name="homeRoute"
              control={roleControl}
              render={({ field }) => (
                <SelectPicker
                  {...field}
                  size="md"
                  style={{ width: "100%" }}
                  data={routePickerData.filter((r) =>
                    watch("routes").includes(r.value),
                  )}
                  labelKey="label"
                  valueKey="value"
                  placeholder={t("roles.forms.homeRoutePlaceholder")}
                />
              )}
            />
            <label>Permisos específicos</label>
            {fields.map((item, index) => (
              <div className="flex justify-start mb-3 gap-3" key={item.entity}>
                <div className="dark:bg-gray-700 bg-gray-100 flex flex-col p-3 rounded-md w-40">
                  <span className="text-xs">Entidad</span>
                  <span className="font-bold">{item.entity}</span>
                </div>
                <div className="dark:bg-gray-700 bg-gray-100 flex flex-col p-3 rounded-md">
                  <span className="text-xs">All</span>
                  <span className="font-bold">
                    <Controller
                      name={`permissions.${index}.manage`}
                      control={roleControl}
                      render={({ field }) => (
                        <Toggle
                          {...field}
                          checked={field.value}
                          onChange={(value) => {
                            setValue(`permissions.${index}.create`, value);
                            setValue(`permissions.${index}.read`, value);
                            setValue(`permissions.${index}.update`, value);
                            setValue(`permissions.${index}.delete`, value);
                            field.onChange(value);
                          }}
                        />
                      )}
                    />
                  </span>
                </div>

                <div className="dark:bg-gray-700 bg-gray-100 flex flex-col p-3 rounded-md">
                  <span className="text-xs">Create</span>
                  <span className="font-bold">
                    <Controller
                      name={`permissions.${index}.create`}
                      control={roleControl}
                      render={({ field }) => (
                        <Toggle
                          {...field}
                          checked={field.value}
                          disabled={watch(`permissions.${index}.manage`)}
                        />
                      )}
                    />
                  </span>
                </div>

                <div className="dark:bg-gray-700 bg-gray-100 flex flex-col p-3 rounded-md">
                  <span className="text-xs">Read</span>
                  <span className="font-bold">
                    <Controller
                      name={`permissions.${index}.read`}
                      control={roleControl}
                      render={({ field }) => (
                        <Toggle
                          {...field}
                          checked={field.value}
                          disabled={watch(`permissions.${index}.manage`)}
                        />
                      )}
                    />
                  </span>
                </div>

                <div className="dark:bg-gray-700 bg-gray-100 flex flex-col p-3 rounded-md">
                  <span className="text-xs">Update</span>
                  <span className="font-bold">
                    <Controller
                      name={`permissions.${index}.update`}
                      control={roleControl}
                      render={({ field }) => (
                        <Toggle
                          {...field}
                          checked={field.value}
                          disabled={watch(`permissions.${index}.manage`)}
                        />
                      )}
                    />
                  </span>
                </div>

                <div className="dark:bg-gray-700 bg-gray-100 flex flex-col p-3 rounded-md">
                  <span className="text-xs">Delete</span>
                  <span className="font-bold">
                    <Controller
                      name={`permissions.${index}.delete`}
                      control={roleControl}
                      render={({ field }) => (
                        <Toggle
                          {...field}
                          checked={field.value}
                          disabled={watch(`permissions.${index}.manage`)}
                        />
                      )}
                    />
                  </span>
                </div>
              </div>
            ))}
          </Col>
          <Col md={24} className="mb-3">
            <div className="flex p-6 relative drawer-button-wrapper">
              <Button
                appearance="default"
                className="rs-btn-big"
                onClick={closeDrawer}
                loading={updateRoleLoading}
              >
                <span className="">{t("buttons.cancel")}</span>
              </Button>
              <Button
                appearance="primary"
                className="ml-2 rs-btn-big"
                type="submit"
                loading={createRoleLoading}
              >
                {t("buttons.save")}
              </Button>
            </div>
          </Col>
        </Row>
      </form>
    </div>
  );
};

export default CreateOrUpdateRole;
