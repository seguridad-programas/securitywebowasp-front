
import * as React from 'react';
import { Checkbox,CheckboxGroup,Button } from 'rsuite';
interface IPageProps {}

const Step3 = (props: IPageProps) => {
  const [checkedOne, setCheckedOne] = React.useState(false);
  const [checkedTwo, setCheckedTwo] = React.useState(false);
  const handleChangeOne = () => {
    setCheckedOne(!checkedOne);
  };

  const handleChangeTwo = () => {
    setCheckedTwo(!checkedTwo);
  };
  return (
    <div>
    <br></br>

    <CheckboxGroup name="checkboxList">
    <p>1- Cual de las siguientes opciones ayuda a  evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="A">Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="B">Instalar antivirus</Checkbox>
    <p>2- Cual de las siguientes opciones ayuda a evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="C">Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="D" >
    Instalar antivirus
    </Checkbox>
    <p>2- Cual de las siguientes opciones ayuda a evitar la vulnerabilidad de control de acceso</p>
    <Checkbox value="E">Autenticación y autorización adecuadas</Checkbox>
    <Checkbox value="F" >
    Instalar antivirus
    </Checkbox>
  </CheckboxGroup>
  <br></br>
  <Button color="cyan" appearance="primary">
        Enviar
      </Button>
    </div>
  );
};

export default Step3;
