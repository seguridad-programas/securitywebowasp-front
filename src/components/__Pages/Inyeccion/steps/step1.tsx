import { Panel } from "rsuite";
import { List } from 'rsuite';
interface IPageProps {}

const Step1 = (props: IPageProps) => {
  return (
    <div>


      <Panel className="bg-gray-900">
              <div className="relative isolate overflow-hidden bg-white px-6 py-24 sm:py-32 lg:overflow-visible lg:px-0">

                <div className="mx-auto grid max-w-2xl grid-cols-1 gap-y-16 gap-x-8 lg:mx-0 lg:max-w-none lg:grid-cols-1 lg:items-start lg:gap-y-10">
                  <div className="lg:col-span-2 lg:col-start-1 lg:row-start-1 lg:mx-auto lg:grid lg:w-full lg:max-w-7xl lg:grid-cols-2 lg:gap-x-8 lg:px-8">
                    <div className="lg:pr-4">
                      <div className="lg:max-w-lg">
                        <p className="text-base font-semibold leading-7 text-indigo-600">Owasp Top 10</p>
                        <h1 className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Inyección</h1>
                        <p className="mt-6 text-xl text-justify leading-8 text-gray-700">Se refiere a la inyección de código malicioso en las aplicaciones web. Esta vulnerabilidad ocurre cuando 
                        los datos de entrada proporcionados por el usuario no son validados o sanitizados adecuadamente, lo que permite a los atacantes insertar código malicioso en la aplicación</p>
                      </div>
                    </div>
                  </div>
                  <div className="-mt-12 -ml-12 p-12 lg:sticky lg:top-4 lg:col-start-2 lg:row-span-2 lg:row-start-1 lg:overflow-hidden">
                    <div className="w-[48rem] max-w-none rounded-xl bg-gray-900 shadow-xl ring-1 ring-gray-400/10 sm:w-[57rem]"></div> 
                    <img src="https://impulso06.com/wp-content/uploads/2023/02/tipos-de-amenazas-ciberseguridad.png" height="400" width="400"/> <br></br>
                    <br></br><br></br><br></br> <br></br><img src="https://owasp.org/Top10/assets/mapping.png" height="500" width="500"/>
                  </div>
                  <div className="lg:col-span-2 lg:col-start-1 lg:row-start-2 lg:mx-auto lg:grid lg:w-full lg:max-w-7xl lg:grid-cols-2 lg:gap-x-8 lg:px-8">
                    <div className="lg:pr-4">
                      <div className="max-w-xl text-base leading-7 text-gray-700 text-justify lg:max-w-lg">
                        <p>Los ejemplos comunes de inyección de código malicioso incluyen la inyección de SQL (SQL injection), la inyección de comandos (command injection), la inyección de 
                          código JavaScript (cross-site scripting o XSS), entre otros. Para prevenir la inyección de código malicioso, es importante validar y sanitizar adecuadamente los datos 
                          de entrada, utilizando técnicas como la validación de formato, la codificación de caracteres especiales y la limitación de la entrada de datos a valores aceptables</p>
                        <ul role="list" className="mt-8 space-y-8 text-gray-600">
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M5.5 17a4.5 4.5 0 01-1.44-8.765 4.5 4.5 0 018.302-3.046 3.5 3.5 0 014.504 4.272A4 4 0 0115 17H5.5zm3.75-2.75a.75.75 0 001.5 0V9.66l1.95 2.1a.75.75 0 101.1-1.02l-3.25-3.5a.75.75 0 00-1.1 0l-3.25 3.5a.75.75 0 101.1 1.02l1.95-2.1v4.59z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">¿ Cuándo la aplicación es vulnerable a este tipo de ataque?</strong>
                            <List className= "overflow-hidden bg-white" bordered>
                                <List.Item style={{backgroundColor: "#FFFFFF"}}>Los datos proporcionados por el usuario no son validados,
                                   filtrados ni sanitizados por la aplicación.</List.Item>
                                <List.Item style={{backgroundColor: "#FFFFFF"}}>Se invocan consultas dinámicas o no parametrizadas,
                                   sin codificar los parámetros de forma acorde al contexto</List.Item>
                                <List.Item style={{backgroundColor: "#FFFFFF"}}>Se utilizan datos dañinos dentro de los parámetros de búsqueda en 
                                  consultas Object-Relational Mapping (ORM), para extraer registros adicionales sensibles</List.Item>
                                <List.Item style={{backgroundColor: "#FFFFFF"}}>Se utilizan datos dañinos directamente o se concatenan, de modo que el SQL o comando resultante 
                                  contiene datos y estructuras con consultas dinámicas, comandos o procedimientos almacenados</List.Item>
                              </List>

                              </span>
                          </li>
                          <li className="flex gap-x-3">
                            <svg className="mt-1 h-5 w-5 flex-none text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z" clip-rule="evenodd" />
                            </svg>
                            <span><strong className="font-semibold text-gray-900">¿ Cuáles son las inyecciones más comunes ? </strong>
                            Algunas de las inyecciones más comunes son SQL, NoSQL, comandos de sistema operativo, Object-Relational Mapping (ORM), LDAP,
                             expresiones de lenguaje u Object Graph Navigation Library (OGNL). El concepto es idéntico para todos los intérpretes.
                             La revisión del código fuente es el mejor método para detectar si las aplicaciones son vulnerables a inyecciones. 
                             Las pruebas automatizadas en todos los parámetros, encabezados, URL, cookies, JSON, SOAP y XML son fuertemente recomendados.
                              Las organizaciones pueden incluir herramientas de análisis estático (SAST), dinámico (DAST) o interactivo (IAST) en sus 
                              pipelines de CI/CD con el fin de identificar fallas recientemente introducidas, antes de ser desplegadas en producción</span>
                          </li>
            
                        </ul>
                        <p className="mt-8">Además, es importante asegurarse de que los controles de acceso se implementen en todas las partes del sistema, 
                        incluyendo la capa de presentación, la capa de aplicación y la capa de base de datos.</p>
                        <h2 className="mt-16 text-2xl font-bold tracking-tight text-gray-900">Problemas de Inyección </h2>
                        <p className="mt-6">Id orci tellus laoreet id ac. Dolor, aenean leo, ac etiam consequat in. Convallis arcu ipsum urna nibh. Pharetra, euismod vitae interdum mauris enim, consequat vulputate nibh. Maecenas pellentesque id sed tellus mauris, ultrices mauris. Tincidunt enim cursus ridiculus mi. Pellentesque nam sed nullam sed diam turpis ipsum eu a sed convallis diam.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

      </Panel>
    </div>
  );
};

export default Step1;
