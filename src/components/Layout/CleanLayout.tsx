const CleanLayout = ({ children }: any) => (
  <div className="container-fluid overflow-hidden">{children}</div>
);

export default CleanLayout;
