import { IRoutePath } from "types/Role.type";
import { ERoutePath } from "./routes";

export const MAIN_MENU_LISTS: IRoutePath[] = [
  { route: ERoutePath.LANDING, childrens: [], displayName: "Landing" },
  {
    icon: "home",
    displayName: "Home",
    childrens: [
      {
        route: ERoutePath.COMPONENTS,
        childrens: [],
        displayName: "Components",
      },
    ],
  },
  { route: ERoutePath.OWASP, childrens: [], displayName: "Owasp" },
  {
    icon: "home",
    displayName: "Home",
    childrens: [
      {
        route: ERoutePath.COMPONENTS,
        childrens: [],
        displayName: "Components",
      },
    ],
  },
  { route: ERoutePath.PROFILE, childrens: [], displayName: "Profile" },
  {
    displayName: "Config",
    icon: "site_config",
    childrens: [
      { route: ERoutePath.USERS, childrens: [], displayName: "Users" },
      { route: ERoutePath.SITE_CONFIG, childrens: [], displayName: "Config" },
      { route: ERoutePath.ROLES, childrens: [], displayName: "Roles" },
      {
        route: ERoutePath.VENDORS,
        childrens: [],
        displayName: "Vendor",
      },
    ],
  },
];
