import { LAYOUTS } from "components/Layout/Components/Layout.types";
import { FC } from "react";
import { ELayout } from "settings/constants";

interface IRouteLayout {
  layout: ELayout;
}

export const RouteLayout: FC<IRouteLayout> = (props) => {
  const { children } = props;
  const { layout = ELayout.CLEAN } = props;
  const Layout = LAYOUTS[layout];

  return <Layout>{children}</Layout>;
};
