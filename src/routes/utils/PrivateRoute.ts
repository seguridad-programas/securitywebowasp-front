import { useRouter } from "next/router";
import React, { cloneElement, isValidElement, useEffect } from "react";
import { ERoutePath, PUBLIC_ROUTES } from "routes/routes";

import { useAuth } from "../../context/auth";

const PrivateRoute = ({ children }) => {
  const router = useRouter();
  const { isAuthenticated } = useAuth();
  // * Redirect to home if not logged in2
  useEffect(() => {
    if (!isAuthenticated()) {
      router.replace({
        pathname: PUBLIC_ROUTES[ERoutePath.LOGIN].path,
        query: {
          redirect: router.asPath,
        },
      });
    }
  }, [isAuthenticated]);

  return React.Children.map(children, (child) => {
    if (isValidElement(child)) {
      return cloneElement(child);
    }

    return child;
  });
};

export default React.memo(PrivateRoute);
