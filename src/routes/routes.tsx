import { ELayout } from "settings/constants";

export enum ERoutePath {
  COMPONENTS = "COMPONENTS",
  PROFILE = "PROFILE",
  SITE_CONFIG = "SITE_CONFIG",
  LANDING = "LANDING",
  OWASP = "OWASP",
  PCONTROLACCESO = "PCONTROLACCESO",
  NOTFOUND = "NOTFOUND",
  LOGIN = "LOGIN",
  ROLES = "ROLES",
  USERS = "USERS",
  RESET = "RESET",
  VENDORS = "VENDORS",
  INYECCION= "INYECCION",
  DISENOINSEGURO = "DISENOINSEGURO",
  FSOFTWAREINTEGRIDADDATOS="FSOFTWAREINTEGRIDADDATOS",
  FIDENTIFICACIONAUTENTICACION = "FIDENTIFICACIONAUTENTICACION",
  CVULNERABLESDESACTUALIZADOS = "CVULNERABLESDESACTUALIZADOS",
  FREGISTROMONITOREO="FREGISTROMONITOREO",
  CSEGURIDADINCORRECTO="CSEGURIDADINCORRECTO",
  FALLASCRIPTOGRFICAS = "FALLASCRIPTOGRAFICAS",
  FSOLICITUDESSERVIDOR ="FSOLICITUDESSERVIDOR",
  SQLTOP10 = "SQLTOP10",
}

export interface IRoute {
  path: string;
  icon?: string;
  layout?: ELayout;
  name: ERoutePath;
}

export const PRIVATE_ROUTES = {
  [ERoutePath.COMPONENTS]: {
    path: "/app/components",
    icon: "components",
    layout: ELayout.MANAGER,
    name: ERoutePath.COMPONENTS,
  },
  [ERoutePath.PROFILE]: {
    path: "/app/profile",
    icon: "profile",
    layout: ELayout.MANAGER,
    name: ERoutePath.PROFILE,
  },
  [ERoutePath.SITE_CONFIG]: {
    path: "/app/config/site-config",
    icon: "config",
    layout: ELayout.MANAGER,
    name: ERoutePath.SITE_CONFIG,
  },
  [ERoutePath.ROLES]: {
    path: "/app/config/roles",
    icon: "roles",
    layout: ELayout.MANAGER,
    name: ERoutePath.ROLES,
  },
  [ERoutePath.USERS]: {
    path: "/app/home/users",
    icon: "users",
    layout: ELayout.MANAGER,
    name: ERoutePath.USERS,
  },
};

export const PUBLIC_ROUTES = {
  
  [ERoutePath.PCONTROLACCESO]: {
    path: "/fsolicitudesservidor",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.FSOLICITUDESSERVIDOR,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/fregistromonitoreo",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.FREGISTROMONITOREO,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/fsoftwareintegridaddatos",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.FSOFTWAREINTEGRIDADDATOS,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/fidentificacionautenticacion",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.FIDENTIFICACIONAUTENTICACION,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/cvulnerablesdesactualizados",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.CVULNERABLESDESACTUALIZADOS,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/cseguridadincorrecto",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.CSEGURIDADINCORRECTO,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/disenoinseguro",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.DISENOINSEGURO,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/inyeccion",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.INYECCION,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/fallascriptograficas",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.FALLASCRIPTOGRFICAS,
  },
  [ERoutePath.PCONTROLACCESO]: {
    path: "/pcontrolacceso",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.PCONTROLACCESO,
  },
  [ERoutePath.OWASP]: {
    path: "/owaspinfo",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.OWASP,
  },
  [ERoutePath.SQLTOP10]: {
    path: "/sqltop10",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.SQLTOP10,
  },
  [ERoutePath.LANDING]: {
    path: "/landing",
    icon: "landing",
    layout: ELayout.LANDING,
    name: ERoutePath.LANDING,
  },
  [ERoutePath.VENDORS]: {
    path: "/app/config/vendors",
    icon: "vendors",
    layout: ELayout.MANAGER,
    name: ERoutePath.VENDORS,
  },
  [ERoutePath.NOTFOUND]: {
    path: "/404",
    icon: "404",
    layout: ELayout.CLEAN,
    name: ERoutePath.NOTFOUND,
  },
  [ERoutePath.LOGIN]: {
    path: "/login",
    icon: "login",
    layout: ELayout.CLEAN,
    name: ERoutePath.LOGIN,
  },
  [ERoutePath.RESET]: {
    path: "/recover",
    icon: "recover",
    layout: ELayout.CLEAN,
    name: ERoutePath.RESET,
  },
};

// * add here routes for that users don't have role assigned
export const DEFAULT_ROUTES: ERoutePath[] = [ERoutePath.PROFILE];

export const ALL_ROUTES = {
  ...PUBLIC_ROUTES,
  ...PRIVATE_ROUTES,
};
